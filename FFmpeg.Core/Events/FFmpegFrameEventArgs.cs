using System;

namespace FFmpeg.Core
{
    public class FFmpegFrameEventArgs : EventArgs
    {
        public uint Frame { get; private set; }
        public uint Fps { get; private set; }
        public float Speed { get; private set; }

        public FFmpegFrameEventArgs(uint frame, uint fps, uint speed)
        {
            Frame = frame;
            Fps = fps;
            Speed = speed;
        }
    }
}