namespace FFmpeg.Core
{
    public enum FFmpegResultType
    {
        Failed,
        Cancelled,
        Complete
    }
}