﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FFmpeg.Core
{
    public class FFmpegProcess : IDisposable
    {
        private ProcessStartInfo _processStartInfo;

        private Process _process;
        private bool disposing = false;

        public bool Running { get { return _process != null && !_process.HasExited; } }

        public FFmpegProcess(string executablePath, string workingDirectory)
        {
            _processStartInfo = new ProcessStartInfo(executablePath)
            {
                CreateNoWindow = true,
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardOutput = true,
                StandardErrorEncoding = Encoding.UTF8,
                WorkingDirectory = workingDirectory
            };
        }

#region Process handling
        public async Task<string[]> GetHardwareAccellerations()
        {
            List<string> result = new List<string>();
            bool startReading = false;
            DataReceivedEventHandler handler = new DataReceivedEventHandler(
                (object sender, DataReceivedEventArgs args) =>
                {
                    if (args.Data == "Hardware acceleration methods:")
                        startReading = true;

                    if (!startReading || string.IsNullOrWhiteSpace(args.Data))
                        return;

                    result.Add(args.Data);
                });
            await execute("-hwaccels", handler);

            return result.ToArray();
        }
        public Task Execute(string arguments, CancellationToken cancellationToken = default(CancellationToken))
                => execute(arguments, handleOutputData);
        protected Task execute(string arguments, DataReceivedEventHandler handler, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (Running)
                throw new InvalidOperationException("This process instance is already running.");

            _processStartInfo.Arguments = arguments;
            using (Process process = Process.Start(_processStartInfo))
            {
                _process = process;
                process.OutputDataReceived += handler;
                process.BeginErrorReadLine();

                while(!process.HasExited)
                {
                    if (cancellationToken.IsCancellationRequested)
                        process.Close();

                    process.WaitForExit(100);
                }
            }
            return Task.FromResult(true);
        }

        private void handleOutputData(object sender, DataReceivedEventArgs args)
        {
#if DEBUG
            Console.WriteLine(args.Data);
#endif
        }

        protected void appendOutputHandler(DataReceivedEventHandler appendableHandler)
        {
            if (Running)
                throw new InvalidOperationException("Cannot append handler to a non-running instance");

            _process.OutputDataReceived += appendableHandler;
        }
#endregion
        public void Dispose()
        {
            if (disposing)
                return;

            disposing = true;
            _process.Dispose();
        }

        public event EventHandler<FFmpegCompletedEvent> CompletedEvent;
        public event EventHandler<FFmpegDataReceivedEvent> DataReceivedEvent;
        public event EventHandler<FFmpegFrameEventArgs> FrameEvent;
    }
}
